add_library('minim')
from ddf.minim import *
from ddf.minim.analysis import *

shapes = []   
circles = [] 
song = None
beat = None
piano = None
bass = None
trigger = None
background_blue = 0
sign = "addition"

def setup():
    # Canvas Setup
    size(945, 945)
    background(0)
    frameRate(60) 
    
    # Sound Setup and Play
    global Minim, BeatDetect, piano, beat, bass, trigger
    minim = Minim(this)
    piano = minim.loadFile("piano.wav")
    beat = BeatDetect()
    bass = minim.loadFile("bass.wav")
    trigger = minim.loadFile("trigger.wav")
    
def mouseClicked():
    global piano, bass, trigger
    piano.play()
    bass.play()
    trigger.play()
    trigger.mute()
    
class Shape():
    
    def __init__(self):
        self.opacity = 100        
        self.cord_x = random(0, 800)
        self.cord_y = random(0, 800)
        self.INIT_SHAPE_WIDTH = random(100, width - self.cord_x - 10)
        self.INIT_SHAPE_HEIGHT = random(100, height - self.cord_y - 10)
        self.width_increase = random(3, 9)
        self.height_increase = random(3, 10)
        
        self.r = random(255)
        self.g = random(255)
        self.b = random(255)
          
        self.r_fill = random(255)
        self.g_fill = random(255)
        self.b_fill = random(255)  
    
    def draw_shape(self):
        colorMode(RGB, 100);
        if self.opacity <= 0:
            fill(self.r_fill, self.g_fill, self.b_fill, 40)
        else:
            fill(self.r, self.g, self.b, self.opacity)
        strokeWeight(5)
        stroke(self.r,self.g,self.b)
        rect(self.cord_x, self.cord_y, self.INIT_SHAPE_WIDTH, self.INIT_SHAPE_HEIGHT)
        
class Circle():
    
    def __init__(self):
        self.x = random(2, 945)
        self.y = random(2, 945)
        
        self.WIDTH = 3
        self.HEIGHT = 3
                
    def draw_circles(self):
        fill(255,255,255)
        noStroke()
        ellipse(self.x, self.y, self.WIDTH, self.HEIGHT)

def calc_background_color():
    global background_blue, sign
    if background_blue >= 20:
        sign = "subtraction"
    elif background_blue <= 0:
        sign = "addition"
    
    if sign == "addition":
        background_blue += 0.23
    elif sign == "subtraction":
        background_blue -= 0.23
        
    return background(0,0,background_blue)
    

def draw():
    global beat, trigger, circles
    # println(frameRate);
    
    if trigger.isPlaying():
        calc_background_color()
    else:
        background(0)
    
    beat.setSensitivity(50)
    beat.detect(trigger.mix)
    
    if beat.isOnset():
        if len(circles) > 0:
            circles = []
        for i in range(1, 100):
            circles.append(Circle())
        shapes.append(Shape())
        
    for circle in circles:
        circle.draw_circles()

    for i, shape in enumerate(shapes):
        shape.opacity -= 5
        # shape.INIT_SHAPE_WIDTH += shape.width_increase
        # shape.INIT_SHAPE_HEIGHT += shape.height_increase
        shape.draw_shape()

        # if shape.opacity <= 0:
        #     # shapes.pop(i)
        #     shape.r_fill + 10
        #     shape.g_fill + 10
        #     shape.b_fill + 10
            
        # if len(shapes) >= 6:
        #     shapes.pop(0)
            
